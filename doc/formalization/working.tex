\documentclass[10pt]{article}
\usepackage[utf8]{inputenc}
\bibliographystyle{ieeetr}
\usepackage{csquotes}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{tikz}


\begin{document}
\title{Formalization of Borůvka's minimum spanning tree algorithm}
\author{Nicolas Robinson-O'Brien}
\date{}
\markboth{UC}{}
\maketitle

\section{Introduction}
We begin our attempt to formalize Borůvka's algorithm by looking at the translation of his original paper given in \cite{oboruvka}.

We start with the simplest description given in that paper which is found in \S 7.

Given a graph, $g$ we let all edges be uncolored and initialize the vertices as a forest, $f$, of trivial blue trees.
We repeat the following step until only one blue tree remains:
\begin{enumerate}
	\item For every blue tree $t$, select the minimum-weight uncolored edge incident to $t$. Color all selected edges blue.
\end{enumerate}

It is noted that there is a potential problem with the termination condition. That is, if the graph is not connected, the algorithm will not terminate under its current description. This requires further investigation, though potentially this could be solved by adding an additional condition that there still exists some edge incident to $n \geq 2$ trees.

\section{Steps}
A tentative list of things that we may need to do:
\begin{enumerate}
	\item Determine a good way to represent a forest. Consider that we want a mechanism by which we will be able to enumerate each tree in the forest.
	\item Determine how we can construct a set of incident edges to a tree, that is those edges which have one node in the tree, and another node not in the tree but in the graph.
	\item Determine how we can color the graph.
	\item Verify the termination condition for unconnected graphs.
	\item Ensure that we handle directed edges correctly.
	\item An expression to ensure that all edge weights are distinct.
\end{enumerate}


\subsection{Forest Representation}
It would be best to maintain the existing representation for a forest, a relation. Composition with $\top$ will give a vector representing the nodes of the forest.

\subsection{Coloring}
Coloring could be achieved by maintaining a variable of edges which make up the blue forest. Initially $\bot$. To this end, it seem like the iterative step of the algorithm could be usefully rewritten as: 

\begin{displayquote}
For every blue tree, $t$, select the minimum-weight uncolored edge incident to $t$. Add these edges to the output forest.
\end{displayquote}


\subsection{Incident Edges}
\label{incident-edges}
We can use work from \cite{guttmann2016relation}, for our expression of edges incident to a given component.

Given a component of the graph, $c$, were $c$ is expressed as a vector containing the nodes of the component, the formulation $c\overline{c}^\top \sqcap g$ gives the incident edges of $c$.

We select the minimum outgoing edge from this expression using the $m$ operation, also described in \cite{guttmann2016relation} and add this to the output forest. This step of the algorithm would then be
\begin{align*}
e &\leftarrow m(c\overline{c}^\top \sqcap g)\\
f &\leftarrow f \sqcup e
\end{align*}

\subsection{Termination Condition - Outer Loop}
First we consider halting upon the condition that there is only one blue tree remaining. This will occur when our output forest is a tree, and every node from the original graph is connected in that tree. It may be difficult to work with an expression that checks these things, though we note that the output being a tree is one of the properties that we wish to prove.

We could maintain a variable, $i$, of all nodes from the graph. Each time we add an edge to the output forest, we remove the nodes that edge connects from $i$. Termination of the loop would occur when $i = \bot$.

This termination condition would then be
\begin{align*}
&i \leftarrow \top\\
&\textbf{while} \ i \ne \bot \ \textbf{do}
\end{align*}

Assume that we have added an edge, $e$, to the output forest. Now we wish to remove the nodes that $e$ connects from $i$. The node from which $e$ starts is given by $e\top$ and the node at which $e$ terminates is given by $e^\top \top$. So to remove these nodes from $i$ we can use

\begin{align*}
i \leftarrow i \sqcap \overline{(e \sqcup e^\top) \top}
\end{align*}

Alternatively, we could initialize $i = \bot$, add nodes to $i$ as the are encountered, and terminate when $i = \top$. This could simplify the expression that maintains $i$ within the loop.

\begin{align*}
i \leftarrow i \sqcup (e \sqcup e^\top) \top
\end{align*}

The algorithm iterates each component and adds the minimum-weight incident edge for each. This could mean that the above expression could be further simplified to

\begin{align*}
i \leftarrow i \sqcup e \top
\end{align*}

The reason for this is that any incident edge will have two components, and the above expression will add the start node, that is the one originating in the given component, to $x$.

	\begin{figure}
		\centering
		\begin{tikzpicture}[auto]
		\node[circle, fill, outer sep=5pt]			(1) at (0,0)	{};
		\node[circle, fill, outer sep=5pt]			(2) at (2,0)	{};
	
		\node[circle, fill, outer sep=5pt]			(3) at (0,-4)	{};
		\node[circle, fill, outer sep=5pt]			(4) at (2,-4)	{};
		
		\node			(5) at (-1.5,0)	{$c_1$};
		\node			(6) at (-1.5,-4)	{$c_2$};

		\path           (1) edge node {} (2);
		\path           (3) edge node {} (4);
		\path[dashed]           (1) edge node {e} (3);
		
		\draw (1, 0) ellipse (2 and 1);
		\draw (1, -4) ellipse (2 and 1);
	
		\end{tikzpicture}
		\caption{Adding an edge, $e$, between two components.}\label{fig:two-components}
	\end{figure}
	
\paragraph{CORRECTION} \textbf{This does not work, after one iteration all nodes will be removed.}
\linebreak

We want to end the outer loop when there are no more edges between components in our blue forest. This may or may not be when the forest is a tree. Initially this set of edges is $g$, that is the input graph consists of $n$ unconnected trivial trees ($n$ components) and all edges in $g$ are between those components.

If we maintain a variable representing the edges we still need to check, $i$, we can update this within the inner loop as the edges between the components are checked.

We will consider all edges between two components, $c_1$ and $c_2$. After selecting the minimal-weighted edged between $c_1$ and $c_2$ and adding it to our output forest we can discard all edges considered from $i$.

\begin{align*}
& i \leftarrow \overline{\overline{g}} \\
& \textbf{while } i \ne \bot \textbf{ do} \\
& \qquad \text{for each component } c \text{ in } f \\
& \qquad \qquad q \leftarrow c\overline{c}^\top \sqcap g \\
& \qquad \qquad e \leftarrow m(q) \\
& \qquad \qquad ... \\
& \qquad \qquad i \leftarrow i \sqcap \overline{q}
\end{align*}

\paragraph{CORRECTION} 
\textbf{This does not work, removing all outgoing edges could remove edges that we need.}
\linebreak

Instead we should remove those outgoing edges which join the components that are also joined by the selected minimal-weight edge, $e$. That would be those edges that start in $c$ and terminate in the component which contains node $e^\top \top$.This  is

\begin{align*}
& g \sqcap c ((f \sqcup f^\top)^{\top^*} e^\top \top)^\top \\
\end{align*}

We make $f$ symmetric with the expression $f \sqcup f^\top$ so as to ensure that when we compute the nodes reachable from the end node, $e^\top \top$, we will capture all nodes in the component as if $f$ were undirected. 

$(f \sqcup f^\top)^* e^\top \top$ is a vector which describes those nodes reachable from node $e^\top \top$ in the forest $f$, as if $f$ were undirected. This is the component that we are interested in finding all edges to, from the component that we are considering in the current iteration.

All possible edges between $c$ and $(f \sqcup f^\top)^* e^\top \top$ can be found by $c((f \sqcup f^\top)^* e^\top \top)^\top$. We take only those edges which are also in our original graph, $g$, to give the final expression which simplifies to

\begin{align*}
& g \sqcap c e (f \sqcup f^\top)^*
\end{align*}

The outer loop variable can now be controlled using

\begin{align*}
& i \leftarrow i \sqcap \overline{g \sqcap c e (f \sqcup f^\top)^*}
\end{align*}

\subsection{Termination Condition - Inner Loop}

We introduce $j$ an inner loop termination variable. It tracks those nodes which still need to be considered in the inner loop. It is reduced by a component at the end of each inner loop.

To choose a component, $c$, in the forest, $f$, we can select an arbitrary edge, $x$, take the start node of that edge, $x\top$, and then compute the relation of nodes reachable from that start node in $f$, $(j \sqcup j^\top)^* x \top$.

\begin{align*}
& j \leftarrow 1 \\
& \textbf{while } j \ne \bot \textbf{ do} \\
& \qquad \qquad x \leftarrow m(j) \\
& \qquad \qquad c \leftarrow (j \sqcup j^\top)^* x \top \\
& \qquad \qquad ... \\
& \qquad \qquad j \leftarrow j \sqcap \overline{c} \\
& \textbf{end} \\
& j \leftarrow f
\end{align*}



\subsection{Directed Edges}
	
We should consider the situation given in Figure \ref{fig:two-components}. We must be careful to avoid adding edge, $e$, to the output forest in both directions, that is from component $c_1$ to component $c_2$ as well as from $c_2$ to $c_1$.

One way to do this is to remove the transpose of any edge that we are adding to our output forest, $f$. The expression from \S \ref{incident-edges} then becomes

\begin{align*}
e &\leftarrow m(c \overline{c}^\top \sqcap g)\\
f &\leftarrow (f \sqcap \overline{e}^\top) \sqcup e
\end{align*}



\subsection{Distinct Edge Weights}

We will take a similar approach to that found in \cite{guttmann2016relation}, in particular the properties given for an m-algebra.

The minimal edge is contained in the graph:
\begin{align*}
m(x) \leq \overline{\overline{x}}
\end{align*}

If the graph is not empty then the result of $m$ is an atom:
\begin{align*}
x \ne \bot \implies m(x) \top m(x)^\top \land m(x)^\top \top m(x) \land \top m(x) \top = \top
\end{align*}

Given some other atom, $y$, which is also in graph $x$, if $y$ is not the minimal edge of $x$ or its transpose, then their weight should not be the same:
\begin{align*}
\forall x, y : y \top y^\top \land y^\top \top y \land \top y \top = \top \land y \sqcap x \ne \bot \land m(x) \ne y \land m(x)^\top \ne y \implies s(m(x)) \ne s(y )
\end{align*}

\begin{enumerate}
	\item Here, $y \top y^\top \land y^\top \top y \land \top y \top = \top$ expresses that $y$ is an atom.
	\item $y \sqcap x \ne \bot$ is to ensure that the atom is in the graph.
	\item $m(x) \ne y \land m(x)^\top \ne y$ is to ensure that we are not comparing the edge to itself, or its transpose given that the graph is undirected and thus symmetric.
\end{enumerate}



\section{The Algorithm}

\subsection{Attempt 1}

\begin{center}
\begin{tabular}{ll}
	1 & $\textbf{input} \ g$ \\
	2 & $i \leftarrow \overline{\overline{g}}$ \\
	3 & $j \leftarrow 1$ \\
	4 & $f \leftarrow \bot$ \\
	5 & $\textbf{while} \ i \ne \bot \ \textbf{do}$ \\
	6 & \qquad $\textbf{while } j \ne \bot \textbf{ do}$ \\
	7 & \qquad \qquad $x \leftarrow m(j)$ \\
	8 & \qquad \qquad $c \leftarrow (j \sqcup j^\top)^* x \top$ \\
	9 & \qquad \qquad $e \leftarrow m(c\overline{c}^\top \sqcap g) $\\
	10 & \qquad \qquad $f \leftarrow (f \sqcap \overline{e}^\top) \sqcup e$ \\ 
	11 & \qquad \qquad $j \leftarrow j \sqcap \overline{c}$ \\
	12 & \qquad \qquad $i \leftarrow i \sqcap \overline{g \sqcap c e (f \sqcup f^\top)^*}$ \\
	13 & \qquad \textbf{end} \\
	14 & \qquad $j \leftarrow f$ \\
	15 & $\textbf{end}$ \\
	16 & $\textbf{output} \ f$\\
	
\end{tabular}
\end{center}

Our first attempt has a number of problems:
\begin{enumerate}
	\item In the inner loop the properties of $j$ change after the first iteration. That is, $j$ is only a tree after one iteration. 
	\item The formulation involving $i$ is quite complex and there should be an easier way to determine whether there are still edges that require processing.
	\item We are not yet considering \textit{flipping} edges when we add $e$ to $f$. the problem is that when we add $e$ joining two components, we do not know that the target of $e$ is a root of the second component, hence adding it could destroy the property that $f$ is a directed forest with roots.
\end{enumerate}

We will find a closed form expression for remaining edges between components which will remove the complexity associated with computing and maintaining $i$.

We will change the inner loop to enumerate components by selecting any node from a vector. To do this, we assign $j = \top$, pick a node from $j$, from that node select its component, process the component, $c$, and then remove the nodes in the $c$ from $j$ while $j$ is not $\bot$. This means that the formulation to find the component will need to change. $c$ will be now be derived from $f$. 

This presents an problem as it means that we will be unable to update $f$ inside the inner loop as that will make the next iteration's selection of a component incorrect. To solve this, three alternative formulations present themselves:
\begin{enumerate}
	\item \textbf{Two inner loops:} while there are still edges between components,  in the first inner loop collect the minimum outgoing edge from each component, in the second loop add these to the forest one at a time. As we have a directed forest then we must ensure that when adding edges we flip any edges required to maintain the properties of the forest. We must in this case also maintain the properties of the edge selection so that we are able to prove, as we add them to $f$, that these edges are the correct ones. 
	\item \textbf{Undirected forest:} similar to the above but without the requirement for a second inner loop. We could have $f$ as an undirected forest in which case we would be able to ignore the second loop and simply add all edges collected, in bulk, once the inner loop exits. We would in this case need to show that there exists a forest, $f_a$, such that $f_a \sqcup {f_a}^\top = f$. This is to show that $f$ is a so called undirected forest.
	\item \textbf{Maintain separate forest variable:} Outside the loop we will assign $f$ to a temporary variable which will be used to compute components for the next sequence of inner loops. $f$ can then be updated as it was previously.
\end{enumerate}

The second approach looks the most interesting and we should revisit that idea. Currently, we proceed with the third option as it seems the most simple to implement.

We will use the formulation given in \cite{guttmann2018verifyingmst} to ensure that $f$ is maintained as a directed forest with roots. We want to make the target of $e$ the new root of the second component. To do this we reverse any path, $p$, from the target of $e$ to the existing root. We also need to modify $f$ to remove any possible reverse edge ($\overline{e}^\top$):

\begin{align*}
f &\leftarrow f \sqcap \overline{e}^\top \\
f &\leftarrow (f \sqcap \overline{f \sqcap \top e f^{\top^*}}) \sqcup ({f \sqcap \top e f^{\top^*}})^\top \sqcup e \\
& \iff  (f \sqcap \overline{\top e f^{\top^*}}) \sqcup ({f \sqcap \top e f^{\top^*}})^\top \sqcup e \\
\end{align*}

\subsection{Attempt 2}

\begin{center}
	\begin{tabular}{ll}
		1 & $\textbf{input} \ g$ \\
		2 & $f \leftarrow \bot$ \\
		3 & $\textbf{while } \overline{{f^\top}^* f^*} \sqcap g \ne \bot \textbf{ do}$ \\
		4 & \qquad $j \leftarrow \top$ \\
		5 & \qquad $f_c \leftarrow f$ \\
		6 & \qquad $\textbf{while } j \ne \bot \textbf{ do}$ \\
		7 & \qquad \qquad $x \leftarrow m(j)$ \\
		8 & \qquad \qquad $c \leftarrow (f_c \sqcup {f_c}^\top)^* x \top$ \\
		9 & \qquad \qquad $e \leftarrow m(c\overline{c}^\top \sqcap g) $\\
		10 & \qquad \qquad $f \leftarrow f \sqcap \overline{e}^\top$ \\
		11 & \qquad \qquad $f \leftarrow (f \sqcap \overline{\top e f^{\top^*}}) \sqcup ({f \sqcap \top e f^{\top^*}})^\top \sqcup e $ \\ 
		12 & \qquad \qquad $j \leftarrow j \sqcap \overline{c}$ \\
		13 & \qquad \textbf{end} \\
		14 & $\textbf{end}$ \\
		15 & $\textbf{output} \ f$\\
		
	\end{tabular}
\end{center}

We briefly describe how each line operates
\begin{enumerate}
	\item The input to the algorithm is a symmetric, weighted, graph ($g$).
	\item  $f$ represents a minimum spanning forest and is initialized as empty.
	\item The expression, $\overline{f^{\top^*}f^*}$ contains all possible edges between components in $f$. This expression is taken from \cite{guttmann2018verifyingmst}. We intersect with $g$ to consider only those edges which exist in the graph. The algorithm will continue while any such edges exist.
	\item $j$ is a temporary variable that tracks those nodes we have considered in our inner loop. It is a vector.
	\item $f_c$ is a temporary variable which we use to maintain a stable representation of $f$ in the inner loop. This is done as we modify $f$ in the inner loop but require a representation of what $f$ was for a number of iterations.
	\item The inner loop will repeat while there are still nodes that have not been processed.
	\item $x$ is a temporary variable that represents an arbitrary edge. It is an atom. We use the minimum function for this.
	\item $c$ is a temporary variable that represents an arbitrary component. It is a vector. It is created by finding those nodes reachable in $f_c$ from $x$, ignoring edge direction.
	\item $e$ is a temporary variable that is the minimum outgoing edge from $c$. It is an atom. the formulation, $m(c\overline{c}^\top \sqcap g)$ was taken from \cite{guttmann2016relation}.
	\item Before adding $e$ to $f$, we remove a potential $e^\top$ so as to mitigate the creation of a cycle.
	\item $e$ is added to $f$, and any paths that must be flipped, are flipped.
	\item We remove all nodes in $c$ from $j$ as they do not need to be considered in the next iteration.
	
	
\end{enumerate}
\paragraph{CORRECTION}

We modify line 8 to $$c \leftarrow {f_c}^{\top *} {f_c}^* x \top$$. The reason for this is to use a  similar expression to line 3 for forest components.

\subsection{Attempt 3}
We look at what the algorithm would be if we were to maintain $f$ as an undirected forest

\begin{center}
	\begin{tabular}{ll}
		1 & $\textbf{input} \ g$ \\
		2 & $f \leftarrow \bot$ \\
		3 & $\textbf{while } \overline{{f^\top}^* f^*} \sqcap g \ne \bot \textbf{ do}$ \\
		4 & \qquad $j \leftarrow \top$ \\
		5 & \qquad $f_c \leftarrow f$ \\
		6 & \qquad $\textbf{while } j \ne \bot \textbf{ do}$ \\
		7 & \qquad \qquad $c \leftarrow {f_c}^* m(j) \top$ \\
		8 & \qquad \qquad $f \leftarrow  m(c\overline{c}^\top \sqcap g) $ \\ 
		9 & \qquad \qquad $j \leftarrow j \sqcap \overline{c}$ \\
		10 & \qquad \textbf{end} \\
		11 & $\textbf{end}$ \\
		12 & $\textbf{output} \ f$\\
		
	\end{tabular}
\end{center}

We no longer need to take care of flipping paths to preserve the properties of a directed forest which simplifies the algorithm.

\bibliography{refs} 
\end{document}