ISABELLE_BIN=/home/user/Isabelle2020/bin

echo compiling tex from proofs...
cd proofs
$ISABELLE_BIN/isabelle build -o browser_info -o document=pdf -o document_output=output -D .

echo moving sources...
cp output/document/WCC.tex ../thesis/appendices/theory
cp output/document/Boruvka.tex ../thesis/appendices/theory

echo compiling tex...
cd ../thesis
pdflatex thesis.tex
bibtex thesis
pdflatex thesis.tex
mv thesis.pdf ../
cd ..
