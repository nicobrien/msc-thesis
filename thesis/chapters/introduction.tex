
\chapter{Introduction}
\label{introduction}

\pagenumbering{arabic}
\setcounter{page}{1}


In 1926, Otakar Borůvka formalized the \gls{mst} problem and proposed a solution to it \cite{oboruvka}. He was perhaps the first person to do so \cite{graham1985history}. Borůvka's \gls{mst} algorithm computes a minimum spanning tree of a weighted, connected, undirected graph whose edge weights are distinct.

Borůvka's original paper is written in Czech; translations of varying completeness can be found in \cite{graham1985history, nevsetvril2001otakar}. The \gls{mst} problem has since been redefined using the language of graph theory that is more readily understandable, for example, in \cite{cormen2009introduction, skiena1998algorithm}.

Borůvka's \gls{mst} algorithm has been independently rediscovered by Choquet \cite{choquet1938etude}, Florek et al.\ \cite{florek1951liaison}, and Sollin \cite{sollin1965trace}. Some of the work discussed in this thesis is based on the identical algorithms presented by these other authors. However, for simplicity, such work will be referred to under the title of \emph{Borůvka's \gls{mst} algorithm}.

\section{A description of Borůvka's MST algorithm}

We use standard terminology to describe the \gls{mst} problem and Borůvka's \gls{mst} algorithm \cite{saidur2017graphtheory}. Recall that a graph is composed of a set of vertices and a set of edges, where each edge joins two vertices. An edge may have a cost associated with it, called the edge's weight. A graph that contains no cycles is a forest. If there is a sequence of one or more edges between all pairs of vertices in the graph then we say the graph is connected. We call a connected graph that contains no cycles a tree. We call a maximal collection of vertices that are connected in a graph a component. Each component of a forest is a tree.

The \gls{mst} problem is concerned with finding a subset of the edges of a graph that form a tree, connecting the graph's vertices, where the sum of the weights of the edges is minimal \cite{skiena1998algorithm}. Since a \gls{mst} algorithm can find the minimal-cost subset of edges that maintains connectivity, the problem has applications in the design of networks, for example, computer, telecommunication, and transportation networks.

Borůvka's \gls{mst} algorithm operates as follows. The algorithm takes, as input, an undirected, connected, distinctly-weighted graph. Next, a forest is initialized with $n$ trees, each containing a single vertex, where $n$ is the number of vertices in the graph. While there is more than one tree in that forest, the following step is repeated. For each tree in the forest, find the edge in the graph with the smallest weight among all edges that leave the tree; all edges found in this way are then added to the forest.

By rephrasing this description, it can solve the more general minimum spanning forest problem, that is, to find a subset of the edges of a graph, $g$, that form a tree for each component of $g$ where the sum of the weights of the edges is minimal. The algorithm starts the same, by initializing a forest to $n$ trees, each containing a single vertex, where $n$ is the number of vertices in the graph. Then, while there are any trees in the forest that could be connected by edges in the graph, the following step is repeated. For each tree that could be connected to another in the forest by an edge in the graph, find the edge with the smallest weight among all edges that leave the tree; all edges found in this way are then added to the forest.

A contemporary description of the implementation of Borůvka's \gls{mst} algorithm can be found in \cite{tarjan1983data}.

\section{Significance of Borůvka's MST algorithm}
\label{introduction:significance-of-the-algorithm}

Typically, algorithm textbooks focus on the \gls{mst} algorithms of Kruskal \cite{kruskal1956shortest}
and Prim \cite{prim1957shortest}, such as in \cite{cormen2009introduction, even2012algorithms, gibbons1985algorithmicgraphtheory}. In \cite{skiena1998algorithm}, the authors classify Borůvka's \gls{mst} algorithm as a less well-known \gls{mst} algorithm and do not give an implementation. However, Borůvka's \gls{mst} algorithm is not merely a historic novelty. It has influenced and been the basis for significant improvements in the running-time complexity of \gls{mst} algorithms.

Often improvements of running-time complexity come by way of the use of different data structures. In 1975, Yao published a modified version of Borůvka's \gls{mst} algorithm with running-time complexity $O(e \cdot \log \log v)$, where $e$ is the number of edges and $v$ is the number of vertices \cite{yao19750}. Fredman and Tarjan \cite{fredman1987fibonacci} found an implementation using Fibonacci heaps with a running-time complexity of $O(e \cdot \beta(e, v))$, where $\beta(e, v) = \text{min}\{i \mid \log^{(i)}v \leq e/v \}$ is a very slowly-growing function similar to the iterated logarithm function \cite{cormen2009introduction}. Their work was improved by Gabow et al.\ to $O(e \cdot \log \beta(e, v))$ \cite{gabow1984efficient}. Our verification will not be concerned with running-time complexity or efficient data structures.

Another promising approach is to make use of parallelism. It is noted that in each iteration of Borůvka's \gls{mst} algorithm, the selection of an edge for each component of the current forest does not depend on any other edge selection. For this reason, the algorithm is well suited for parallelism. Our verification will be of a sequential version of Borůvka's \gls{mst} algorithm, not a parallel version.

Chazelle's \gls{mst} algorithm makes use of Borůvka's \gls{mst} algorithm and has a running-time complexity of $O(e \cdot \alpha(e, v))$, where $\alpha$ is the inverse of Ackermann's function \cite{chazelle2000minimum}. This is a nearly linear running-time complexity. It is not known whether a linear-running-time, deterministic \gls{mst} algorithm exists.

Karger has modified Borůvka's \gls{mst} algorithm to create a randomized \gls{mst} algorithm with an expected linear-running-time complexity $O(e)$ \cite{karger1995randomized}.

\section{Formal verification}

Formal verification is an approach to reliability assurance for computer software where a higher level of guarantee is given by way of reasoning about a specification and a program that is shown to satisfy that specification. A specification describes what is expected of a program.

Often, the reliability of algorithms is managed by manual or automated tests. It is not possible for such an empirical method of reliability testing to provide verification that any but the simplest algorithms are correct. Rather, a failed test will verify that a program is incorrect. Instead, a formal mathematical proof may be used to verify that an algorithm meets its specification, that is, the algorithm will produce the correct output for all possible inputs. The correctness of an algorithm can be formally proved, in the sense that it meets its specification, by expressing the semantics of the algorithm. This amounts to expressing the algorithm and the specification mathematically, and then using logic to show that the mathematical expression of the algorithm satisfies the mathematical expression of the specification.

The associated problem of manual proof-verification may be somewhat alleviated by the use of a program, such as a proof assistant, that is capable of mechanically verifying proofs.

\subsection{Proof assistants}

There are many proof assistants available. See, for example, the survey \cite{boldo2016formalization} and book \cite{wiedijk2006seventeen}. The purpose of such proof assistants is to automatically verify mathematical proofs and in some cases assist with proof finding. Some of the more prominent proof assistants include Coq \cite{DBLP:series/txtcs/BertotC04}, \textit{HOL} \cite{gordon1993introduction}, Mizar \cite{muzalewski1993outline}, and Isabelle/HOL
\cite{nipkow2002isabelle}. All of these proof assistants include proof libraries of formally verified mathematics, for example, sets, relations, natural numbers, and integers.

Coq is an interactive proof assistant that facilitates the production of specifications and programs which are proved to be consistent with those specifications. It implements a mathematical language Gallina, based on the Calculus of Inductive Constructions, that combines higher-order logic and a richly-typed functional programming language. Coq differs from many other proof assistants in that it provides a mechanism to extract a working program from the proof. There are plugins that enhance Coq with automated proof-finding tools, for example, SMTCoq \cite{ekici2017smtcoq}.

\textit{HOL} is an interactive proof assistant for the form of predicate logic by the same name. It is released with built-in proof-finding tools and a mechanical verification system. To avoid confusion we will not make any further references to this proof assistant and in the remainder of this thesis, \gls{hol} should be taken to mean the form of mathematical predicate logic.

Mizar consists of an input language for the formalization of proofs and a mechanical verification tool. The language is close in appearance to mathematics, and therefore lends itself to be more readily understood by a person schooled in mathematics but unfamiliar with Mizar.

Isabelle/HOL has similar features. It allows both manual proof-writing as well as providing proof-finding tools and allowing the use of external proof-finding tools. It has a proof verification system and an expressive language similar to mathematics. It is most sensible for us to use Isabelle/HOL due to the extensive related work that has already been done in this system.

\subsection{Isabelle/HOL}

We chose to use Isabelle/HOL to produce the formal verification of Borůvka's \gls{mst} algorithm. This was primarily because the algebraic framework that our work builds on and a substantial number of relevant lemmas had already been published in the Archive of Formal Proofs, a repository of proofs that have been verified by Isabelle/HOL.

Our Isabelle/HOL theory artifact begins by inheriting useful library files, including a library for Hoare logic proofs and the theory files that include the definition of the algebraic framework that our proof is based in. We work in $m$-$k$-Stone-Kleene relation algebras. This is an algebra based on $m$-Kleene algebras, discussed in Section \ref{background:stone-kleene-relation-algebras}.

Isabelle/HOL provides utilities that simplify proof development. A list of lemmas is presented in a separate pane. Fast navigation hotkeys are available to jump to key definitions and navigation history is maintained so that a user is able to return to their previous editing positions. The proof state at the cursor position, in particular the proof subgoal, is also presented in a separate pane.

We have relied heavily on Sledgehammer, a proof-finding tool that is integrated with Isabelle/HOL \cite{paulson2010sledgehammer}. Sledgehammer attempts to automatically find proofs for goals by making requests to various automatic theorem provers. The proofs that are found are then verified by Isabelle/HOL. Sledgehammer has heuristics to select relevant lemmas that are available in the scope of the proof goal. These lemmas are provided to the automatic theorem provers. In order to discharge proof goals without this tool, the author would need to know the name of the lemmas required that prove each goal. This is a considerable ask, especially for an author who is unfamiliar with the available lemmas that may be spread out over many proof files. While Sledgehammer is not typically able to find proofs for complex goals, it has been a valuable addition.

Finally, we have used a Hoare-logic verification generator library that comes with Isabelle/HOL \cite{nipkow1998winskel, nipkow2002hoare}. This library has allowed us to input our formalized algorithm, from Section \ref{formalization:description}, and have a list of proof goals generated that, once satisfied, imply that the algorithm is correct. We show partial correctness. This means that whenever the input satisfies the precondition and the algorithm terminates, its output satisfies the postcondition. We discuss the work required to prove termination in Chapter \ref{conclusion}.



\section{Aim of this thesis}

The aim of our research is to provide a machine-verified formal partial-correctness proof for Borůvka's \gls{mst} algorithm using Isabelle/HOL. To our knowledge, there is no formal proof of correctness for this algorithm, machine-verified or otherwise.

Guttmann has recently introduced new algebras \cite{guttmann2018verifyingmst} that have proved useful for reasoning about weighted-graph algorithms. In particular, they have been used to complete total-correctness proofs of both Prim's and Kruskal's \gls{mst} algorithms. We intend to use the same algebras to further demonstrate their suitability as a framework for reasoning about weighted graphs in general and constructing proofs for \gls{mst} algorithms in particular.

\section{Organization of this thesis}

In Chapter \ref{background} we discuss mathematical structures that we use in our work.

In Section \ref{background:graphs} we give definitions for various graph structures and concepts that we use throughout the paper. Almost all of these are standard in the literature. Of particular importance is the \emph{rooted directed forest}, a structure that we use often in our proof.

In Section \ref{background:mst-algorithm} we discuss minimum spanning trees in general. We give a definition of Borůvka's \gls{mst} problem in Section \ref{background:boruvkas-mst-algorithm} as well as an example of its operation. Some notable differences to Kruskal's \gls{mst} algorithm are also mentioned.

In Section \ref{background:algebras} we discuss binary relations. There is a straightforward model of a directed graph as a relation. Binary relations provide a good base to reason about unweighted graphs and, with some changes to the algebraic structure, weighted graphs. Our proof is based on an existing algebraic framework and we give definitions for these structures and discuss ideas that are used in the remainder of the thesis.

In Chapter \ref{formalization} we introduce the algebras that our formalization and proof are completed in as well as present and describe our formalization of Borůvka's \gls{mst} algorithm.

In Section \ref{formalization:m-k-kleene-algebras}, we introduce $m$-$k$-Stone-Kleene relation algebras, that our proof is completed in. An $m$-$k$-Stone-Kleene relation algebra combines $m$-Kleene relation algebras, discussed in Section \ref{background:stone-kleene-relation-algebras}, the Tarski rule and a new binary operation, $k$, that models the selection of a component in a graph.

In Section \ref{formalization:description} we present our formalization of Borůvka's \gls{mst} algorithm. We make three changes to the algorithm as it is described in Section \ref{background:boruvkas-mst-algorithm}. The first is that our formalization uses a variable to track a forest. As the algorithm progresses, this forest grows to be the \gls{mst}. The second is that the formalization solves the minimum spanning forest problem. In the case where the input graph is connected the output forest will be a tree. Lastly, we do not require that the input graph's edge weights are distinct. Instead, we predicate the addition of an edge to the forest variable on the condition that such an addition will not create a cycle.

In this thesis, we include the Isabelle/HOL theory in Appendix \ref{appendix:theory}. We do not discuss all the theorems contained in it, though we do discuss a selection of them in Chapter \ref{correctness}.

In Section \ref{correctness:e-forests} we introduce a new abstraction, $E$-forests, that is used in our proof to reason about reachability in the remainder of Chapter \ref{correctness}.

In Section \ref{correctness:conditions-invariants} we formally specify what it means to be a minimum spanning forest, that is, what the output of our formalization should satisfy. Additionally, we give the invariants used to show that the specification is met.

In Section \ref{correctness:proof} we discuss how our invariants are established and maintained. We provide several examples, in various levels of detail.

\section{Contributions}

The main contributions of our work are:
\begin{itemize}
	\item A new algebraic structure, $k$-Stone relation algebras, that extends Stone relation algebras with a component selection operation, $k$. The $k$ operation models selection of components in graphs. We also introduce $m$-$k$-Stone-Kleene relation algebras, an algebraic structure that combines $k$-Stone relation algebras, $m$-Kleene algebras and the Tarski rule. Our formalization of Borůvka's \gls{mst} algorithm and its partial-correctness proof is done in these algebras. These contributions are presented in Section \ref{formalization:m-k-kleene-algebras}.
	\item A formal specification of Borůvka's \gls{mst} algorithm, presented in Section \ref{formalization:description}.
	\item A new abstraction, $E$-forests, that we use to model reachability, presented in Section \ref{correctness:e-forests}.
	\item A Hoare-logic formally-verified partial-correctness proof of Borůvka's \gls{mst} algorithm, discussed in Section \ref{correctness:proof}.
	\item Formal verification of our proof as Isabelle/HOL theorems, included in Appendix \ref{appendix:theory}.
\end{itemize}


\section{Related work}

A formal proof reasons about correctness using a step-by-step argument expressed in a formal language such as mathematics, while an informal proof does not. The correctness of graph algorithms in general, and \gls{mst} algorithms in particular, have typically been argued informally, for example, in \cite{cormen2009introduction, skiena1998algorithm}. Borůvka included such an informal proof of his \gls{mst} algorithm in his original work \cite{oboruvka}.

Relation algebras provide a framework to reason about and develop unweighted-graph algorithms. An unweighted graph has a direct representation as a Boolean matrix and hence a binary relation. Such relations may represent both directed and undirected graphs. It is therefore not surprising that relation algebras have been used to reason about unweighted graph algorithms with some success.

In \cite{kehden2006relation}, the authors use relation algebras to improve computation performance for the minimum vertex cover, maximum clique, and maximum independent set problems. In \cite{berghammer2013computing}, the authors present a relation-algebraic approach to the tournament choice problem. This involves the computation of choice sets using relation algebra and RelView \cite{behnke1998relview, berghammer2005relview}, a software system that assists with computation on Boolean matrices.

In \cite{berghammer1998relation}, the authors use relation algebras to compute spanning trees of undirected graphs. A proof is also given for a variant of Prim's \gls{mst} algorithm using relation algebras. The proof includes arguments about the Galois connection between incidence relations and adjacency relations. While the incidence relations make computation more difficult, they provide a more convenient base to generalize with weighted graphs. No automated proof is provided. However, an implementation of the algorithm is given in RelView. The authors discuss how the use of a formal calculus that allows automated verification would be more ideal than the informal approach taken.

Relation algebras are not so suitable for reasoning about weighted graphs, so some authors have looked to other algebraic frameworks for this purpose. Semirings have been applied to path finding problems in graphs \cite{gondran2008graphs, DBLP:journals/fac/HofnerM12}. Mohri discusses the application of semirings to weighted-graph shortest-path problems in \cite{mohri2002semiring}. The author notes that other authors have also investigated the use of semiring frameworks with respect to the all-pairs shortest-distance problem. A formal proof is given, though it is not machine-verified.

A general algebraic framework for the \gls{mst} problem is introduced in \cite{bistarelli2008c}. It leverages constraint-based semirings and is inspired by the work of Mohri. The paper includes a proof of a variant of Kruskal's \gls{mst} algorithm. The proof is informal and not automated.

In \cite{NipkowEH-ATVA20}, the authors survey recent work formally verifying algorithms from the algorithm textbook by Cormen et al. \cite{cormen2009introduction}.

Use has been made of theorem provers to construct machine-verified proofs for some \gls{mst} algorithms. A distributed \gls{mst} algorithm by Gallagher et al.\ \cite{gallager1983distributed} was formally proved correct by Hesselink \cite{hesselink1999verified} in the Nqthm framework \cite{boyer1995boyer}. Abrial et al.\ use the B~event-based method in the Atelier~B environment to show the correctness of Prim's \gls{mst} algorithm \cite{abrial2003formal}.

A proof, machine-verified by Referee \cite{omodeo2006computerized}, is presented in \cite{omodeo2014set} showing that a connected claw-free graph has a Hamiltonian cycle in its square and, if it has an even number of vertices, owns a perfect matching.

A formal verification has been constructed in \cite{lee2004verification} for Dijkstra's single source shortest path algorithm, Prim's \gls{mst} algorithm, and the Ford-Fulkerson maximum network flow algorithm. It is a machine-verified proof written in Mizar \cite{muzalewski1993outline}.

A library for proving various properties of graph theory is presented in \cite{doczkal2020graph}. The library is written in Coq \cite{DBLP:series/txtcs/BertotC04} and is used to formally verify: Menger's theorem, the excluded-minor characterization of treewidth-two graphs, and a correspondence between multigraphs of treewidth at most two and terms of certain algebras. A formal verification, also written in Coq, of the iterated register coalescing algorithm is presented in \cite{DBLP:conf/esop/BlazyRA10}. This paper includes a proof that the algorithm terminates.

A graph library \cite{noschinski2015graph} has also been constructed for Isabelle/HOL \cite{nipkow2002isabelle}. It covers simple graphs and multigraphs. A formalization of planar graphs was constructed as part of the Flyspeck project \cite{nipkow2006flyspeck} in Isabelle/HOL.

A formal verification is given in Isabelle/HOL for two network flow algorithms, Edmonds-Karp and generic push–relabel, in \cite{lammich2019formalizing}. The time complexity bounds, $O(V E^2)$ and $O(V^2 E)$ respectively, are also formally verified in that paper.

Guttmann presents a Stone-Kleene relation-algebraic framework, in \cite{guttmann2018relation}, and uses it to formally verify Prim's \gls{mst} algorithm. The algebra permits instantiation by weighted matrices. He extends this work in \cite{guttmann2018verifyingmst} including a proof of Kruskal's \gls{mst} algorithm. This is a proof of total correctness, that is, the algorithm is shown to terminate. Use is made of Isabelle/HOL and its integrated, automated theorem provers to mechanically verify these results. The relevant Isabelle/HOL theorems, in particular those of Stone relation algebras \cite{guttmann2017stonerelationalgebra}, Stone-Kleene relation algebras \cite{guttmann2017stonekleenerelationalgebras}, and aggregation algebras \cite{guttmann2018aggregationalgebras}, are included in the Archive of Formal Proofs.

Our work is based on the work by Guttmann and we have been able to reuse many results from his work. The verification of both Prim's and Kruskal's \gls{mst} algorithms use Hoare logic and the majority of both proofs are performed in Stone-Kleene relation algebras. The proof of Kruskal's \gls{mst} algorithm is particularly related to our work because, similar to Borůvka's \gls{mst} algorithm, it is concerned with growing a forest whereas Prim's \gls{mst} algorithm grows a tree. This is discussed further in Section \ref{background:other-mst-algorithms}.

Guttmann gives different formal specifications of the \gls{mst} problem for Prim's and Kruskal's \gls{mst} algorithms. The specification for Prim's \gls{mst} algorithm outputs a tree whereas the specification for Kruskal's \gls{mst} algorithm outputs a forest. Guttmann notes that it should be possible to modify the proof of Prim's \gls{mst} algorithm so that it uses the same specification as for Kruskal's \gls{mst} algorithm since trees are a special case of forests.

We use the minimum spanning forest specification given for Kruskal's \gls{mst} algorithm since a forest structure is more suitable for our work. This specification is discussed in Section \ref{correctness:specification}. The \gls{mst} problem is defined at the start of Section \ref{background:mst-algorithm}.
