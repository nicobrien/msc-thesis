
\chapter{Conclusion}
\label{conclusion}

Our aim was to provide a machine-verified formal partial-correctness proof for Borůvka's \gls{mst} algorithm. We have given a formal description of Borůvka's \gls{mst} algorithm using $m$-$k$-Stone-Kleene relation algebras. We have completed a formal, partial correctness proof to show that this description satisfies a formal specification for computing minimum spanning forests. The proof has been automatically verified by Isabelle/HOL.

\section{Limitations and future work}
\label{conclusion:limitations}

A minor change that could be made to our proof would be to define an $E$-forest in terms that are closer to the way that forests are defined. In Definition \ref{definitions:e-forest}, we describe an $E$-forest as being univalent. This description of a forest is one where the arcs are directed towards the root vertices. However, as discussed in Section \ref{background:stone-kleene-relation-algebras}, we define a forest as being injective, that is, the arcs of the forest are directed away from the root vertices. It should not require too much work to adjust this definition though a number of theorems of the proof would need to be changed. Making this change would result in a more consistent approach to forest definition across the proof.

We do not prove that the algorithm terminates. Rather, our Hoare-logic proof concludes that if the algorithm terminates then the output is a minimum spanning forest of the input graph. We do not expect this to require a substantial amount of time to complete, in particular because of the prior work by Guttmann to extend the Hoare-logic library we are using to allow for total-correctness proofs.

We claim that our formalization, presented in Section \ref{formalization:description}, is an accurate representation of Borůvka's \gls{mst} algorithm, with the exception of an additional conditional statement in the inner while-loop, as discussed in the following paragraph. This claim is based on informal reasoning only so is not made with the same confidence as our partial-correctness proof.

We do not give a specification for the input graph to have distinct arc weights. As discussed in Section \ref{background:boruvkas-mst-algorithm}, this could result in a cycle being created in the algorithm's output. Our formalization circumvents this problem by having a condition in the inner while-loop that checks whether the addition of the arc $e$ would create a cycle in the forest $f$ if it were added. It does this by checking that $e$ is not contained in any component of $f$ and performing a skip operation if it is.

Recall, from Section \ref{introduction:significance-of-the-algorithm}, that one motivation for using Borůvka's \gls{mst} algorithm is the performance gain to be had by leaning on how readily it may be parallelized. One limitation with our approach of using a condition in the inner while-loop that depends on the state of the forest is that it results in a description which is not amenable to parallelization. At least, in practice this would require some synchronization before performing the conditional check.

We suspect that if a specification were given that required the input graph to have only distinct arc weights then we could derive that the selected arc is not contained in a component of the forest in each iteration of the inner while-loop. We could then remove the condition from the formalization.



\section{Discussion}

We have benefited greatly from the prior work of Guttmann, both from the algebraic framework that we extend and from the theorems and lemmas published in the Archive of Formal Proofs. We have found that Stone-Kleene relation algebras are a useful algebraic framework to prove most of our results. Some parts of our proof required additional structure. There were sections of our proof that additionally required the axioms of $m$-Kleene relation algebras, in particular for selecting an arc with minimal weight and for comparing weights between arcs.

We extended Stone relation algebras to $k$-Stone relation algebras with the addition of a component selection operation, $k$. This operation was not strictly necessary to complete our proof and we could have used an algebraic expression in $m$-Kleene relation algebras to formalize the selection of a component. However, we found the addition of the $k$ operation for this purpose to more clearly communicate the desired intent to a reader of the formalization.

While most of our proof used only the axioms of Stone-Kleene relation algebras, we work in $m$-$k$-Stone-Kleene relation algebras to have access to the component selection operation and because we required the Tarski rule to prove that a particular element is an arc.

Because our proof is conducted using only the axioms of $m$-$k$-Stone-Kleene relation algebras, the proof will hold for instances other than the weighted-graph model. We do not explore this further here but note that in \cite{guttmann2016verification} it is discussed how different instances of the $m$-Stone algebras give rise to formalizations of various other algorithms, for example, the minimum bottleneck spanning tree problem. The proof holds for any instance that satisfies the axioms the proof is conducted in. This means that Borůvka's \gls{mst} algorithm is correct for various related \gls{mst} problems.

We benefited from the tools and libraries of Isabelle/HOL. Sledgehammer was able to find proofs for many of the smaller goals for us which alleviated the burden of having to know the name and content of each relevant property in the library of theory files. We used the Hoare-logic verification generator library to generate the proof goals for us directly from our formalization. This meant that we did not have to manually generate our verification conditions and, given that we rewrote our formalization and loop invariants a number of times, saved us considerable time.
