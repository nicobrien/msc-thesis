
\documentclass[11pt,a4paper]{report}


% Packages
\usepackage[]{graphicx}
\usepackage[margin=2.5cm, headheight=13.6pt]{geometry}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{tikz}
\usepackage{glossaries}
\usepackage{color, colortbl}
\usepackage{fancyhdr}
\usetikzlibrary{matrix, fit}
\usepackage[linesnumbered,figure]{algorithm2e}
\usepackage{enumitem}
\usepackage{appendix}
\usepackage[utf8]{inputenc}


% Algorithm indentation
\SetInd{0.5em}{2em}


% Colors
\definecolor{highlightGray}{RGB}{160, 160, 160}
\definecolor{headerGray}{RGB}{80, 80, 80}

% Header
\pagestyle{fancy}
\fancyhf{}
\fancyhead[L]{\textit{\color{headerGray}\thepage}}
\fancyhead[R]{\color{headerGray}\textit{\nouppercase{\leftmark}}}
\renewcommand{\headrulewidth}{0pt}

\fancypagestyle{plain}{%
	\fancyhf{}
	\fancyhead[L]{\textit{\color{headerGray}\thepage}}
}


% When referring to a subfigure \ref{key} we want the output formatted as M(m) rather than Mm.
\renewcommand\thesubfigure{(\alph{subfigure})}
\DeclareCaptionLabelFormat{surround}{#2}
\captionsetup[subfigure]{labelformat=surround}

% TikZ
\usetikzlibrary{calc, shapes, positioning}

% connect will draw a fat rounded border around a start and end node
\tikzset{connect/.style={rounded corners=#1,
		to path= ($(\tikztostart)!-#1!(\tikztotarget)!-#1!-90:(\tikztotarget)$) -- ($(\tikztotarget)!-#1!(\tikztostart)!-#1!90:(\tikztostart)$) --
		($(\tikztotarget)!-#1!(\tikztostart)!#1!90:(\tikztostart)$) --
		($(\tikztostart)!-#1!(\tikztotarget)!-#1!90:(\tikztotarget)$) --
		cycle (\tikztotarget)
}}
\tikzset{connect/.default=7pt}

% Declare background layer, we often use this layer in figures to 'highlight' certain things
\pgfdeclarelayer{bg}

% Set the order of the layers (main is the standard layer)
\pgfsetlayers{bg,main}

% To draw a loop-like arc
% Syntax: [draw options] (center) (initial angle:final angle:radius)
\def\centerarc[#1](#2)(#3:#4:#5)
{ \draw[#1] ($(#2)+({#5*cos(#3)},{#5*sin(#3)})$) arc (#3:#4:#5); }

% Figures
% add '(continued)' to caption for continued figures
\DeclareCaptionFormat{cont}{#1 (continued)#2#3\par}

% Commands
\makeatletter
\newcommand\breakingstruct[1]{%
	\@tempcnta=0
	\langle
	\@for\@ii:=#1\do{%
		\@insertbreakingcomma
		\@ii
	}%
	\rangle
}
\def\@insertbreakingcomma{%
	\ifnum \@tempcnta = 0 \else\,,\ \linebreak[1] \fi
	\advance\@tempcnta\@ne
}
\makeatother

\makeatletter
\newcommand\breakingtuple[1]{%
	\@tempcnta=0
	(
	\@for\@ii:=#1\do{%
		\@insertbreakingcommanospace
		\@ii
	}%
	)
}
\def\@insertbreakingcommanospace{%
	\ifnum \@tempcnta = 0\else, \linebreak[1] \fi
	\advance\@tempcnta\@ne
}
\makeatother


\DeclareMathOperator*{\Eforestpath}{\rightsquigarrow}

% Acronyms
\newacronym{mst}{MST}{Minimum Spanning Tree}
\newacronym{hol}{HOL}{Higher Order Logic}

\makeindex

% Symbols used for relation algebra
\newcommand{\setFont}[1]{\textsf{#1}\normalfont}
\newcommand{\sL}{\setFont{L}}
\newcommand{\sO}{\setFont{O}}
\newcommand{\sI}{\setFont{I}}
\newcommand{\sE}{\setFont{E}}

% Allow our figures to take up bigger portions of a page than the default
\renewcommand\floatpagefraction{.7}
\renewcommand\topfraction{.7}
\renewcommand\bottomfraction{.7}
\renewcommand\textfraction{.3}
\setcounter{totalnumber}{50}
\setcounter{topnumber}{50}
\setcounter{bottomnumber}{50}

% We have unicode symbols in our section and chapter titles, which we want to render correctly
\usepackage[colorlinks=true,linkcolor=black,urlcolor=black,unicode=true]{hyperref}
\urlstyle{rm}


\usepackage{bookmark}


\usepackage{isabelle,isabellesym}
\usepackage{amssymb,ragged2e}


\newcommand{\HRule}{\rule{\linewidth}{.5pt}}

% Variables

\newcommand{\thesistitle}{A formal correctness proof of Borůvka's minimum spanning tree algorithm}
\newcommand{\universityname}{University of Canterbury}
\newcommand{\supervisorname}{Walter Guttmann}
\newcommand{\authorname}{Nicolas Robinson-O'Brien}

\AtBeginDocument{
	\hypersetup{pdftitle=\thesistitle}
	\hypersetup{pdfauthor=\authorname}
}


\begin{document}

% we present the frontmatter with roman page numbering
\pagenumbering{roman}

% To help prevent some blocks of text running into the margins
\emergencystretch 3em%

\begin{titlepage}
	\begin{center}

		\vspace*{.06\textheight}

		{\scshape\LARGE \universityname\par}\vspace{1.5cm}

		\textsc{\Large Master's Thesis}\\[0.5cm]

		\HRule\\[0.4cm]

		{\huge \bfseries \thesistitle\par}\vspace{0.4cm}

		\HRule\\[1.5cm]
		\noindent
		\begin{minipage}[t]{0.5\textwidth}
			\begin{flushleft}
				\large
				\emph{Author:}\\ \authorname
			\end{flushleft}
		\end{minipage}%
		\begin{minipage}[t]{0.5\textwidth}
			\begin{flushright} \large
				\emph{Supervisor:}\\ \supervisorname
			\end{flushright}
		\end{minipage}\\[3cm]

		\vfill

		{\large \today}\\[4cm] % Date

		\includegraphics[scale=0.20]{uc-logo.jpg}

		\vfill

	\end{center}
\end{titlepage}



\begin{abstract}
\thispagestyle{plain}
\setcounter{page}{2}

Prior work has described an algebraic framework for proving the correctness of Prim's and Kruskal's minimum spanning tree algorithms. We prove partial correctness of an additional minimum spanning tree algorithm, Borůvka's, using the same framework. Our results are formally verified using the automated deduction capabilities of the Isabelle proof assistant. This further demonstrates the suitability of the algebraic framework as a sound abstraction for reasoning about weighted-graph algorithms.

\end{abstract}


% The cover page and abstract are pages 1 and 2. We want the PDF
% pages to match those listed in the table of contents
\setcounter{page}{3}

\tableofcontents

\listoffigures

\newtheorem{definition}{Definition}
\newtheorem{theorem}{Theorem}



% The actual content of the thesis is included from the following tex files


\include{./chapters/introduction}
\include{./chapters/background}
\include{./chapters/formalization}
\include{./chapters/proof-of-correctness}
\include{./chapters/conclusion}

\clearpage

\bibliographystyle{plain}
\bibliography{refs}
\addcontentsline{toc}{chapter}{Bibliography}

\begin{appendices}
	\include{./appendices/appendix-intuition-matrix-notation}
	\include{./appendices/appendix-theory}
\end{appendices}




\end{document}
