\chapter{An intuition for the weighted-graph instance notation}
\label{appendix:intuition}

We denote the set of matrices whose entries range over the real numbers, extended by $\top$ and $\bot$ as $\mathbb{R}'^{A \times A}$. Here, we give some intuition for this.

The set of extended real numbers, $\mathbb{R}'$, is the union of the real numbers with the set $\{\bot, \top\}$, that is $\mathbb{R}' = \mathbb{R} \cup \{\bot, \top\}$. A notation that is sometimes used to denote the set of functions from set $X$ to set $Y$ is $Y^X$. Before discussing how this applies for $\mathbb{R}'^{A \times A}$ we give a simple example.

Note that $2^S$ denotes the set of functions mapping elements from the set $S$ to the two-element set. A common use of this particular notation is to denote the power-set relation (function). This is because we can interpret one element of the two-element set to denote inclusion (for instance $1$) and the other to denote exclusion (for instance $0$).


\begin{figure}[h]
	\centering
	\begin{tikzpicture}[auto,x=25pt,y=25pt]
		\begin{scope}[every node/.style={inner sep=0pt, minimum size=5pt, outer sep=10pt}]
			\node[] (10) at ( 0.0,  5.0) {$a$};
			\node[] (11) at ( 0.0,  4.0) {$b$};
			\node[] (12) at ( 0.0,  3.0) {$c$};
			\node[] (13) at ( 0.0,  2.0) {$d$};
			\node[] (20) at ( 4.5,  3.0) {$0$};
			\node[] (21) at ( 4.5,  4.0) {$1$};
		\end{scope}
		\node[] (l1) at ( 0.0,  0.5) {$S$};
		\node[] (l2) at ( 4.5,  0.5) {$\{0, 1\}$};
		\node[] (l3) at ($0.5*(l1) + 0.5*(l2) + (0, 5)$) {$f : S \mapsto \{0, 1\}$};

		\draw ($(11)!0.5!(12)$) ellipse (1.3 and 2.3);
		\draw ($(20)!0.5!(21)$) ellipse (1.3 and 2.3);

		\path [->] (10) edge node {} (21);
		\path [->] (11) edge node {} (21);
		\path [->] (12) edge node {} (21);
		\path [->] (13) edge node {} (20);
	\end{tikzpicture}
	\caption[A depiction of how $2^S$ denotes a power set.]{A depiction of how $2^S$ denotes a power set. Here, $f$ maps $S$ to $\{0, 1\}$ in a way that denotes the subset $\{a, b, c\}$.}
	\label{fig:power-set-intuition}
\end{figure}

Consider the set $S = \{a, b, c, d\}$ and $\{0, 1\}$ in Figure \ref{fig:power-set-intuition}. The instance of $f$ shown maps all elements of $S$ to $1$ except for $d$ which is mapped to $0$. This is the instance of $2^S$ that denotes the subset, $\{a, b, c\}$. The function mapping all elements of $S$ to $0$ would denote the empty set.

Next we consider $\mathbb{R}'^{A \times A}$. Our interpretation of this is the set of functions mapping the set of tuples of the Cartesian product, $A \times A$, to the extended real numbers. Recall, that $A$ denotes the index set of vertices under consideration. Therefore, $\mathbb{R}'^{A \times A}$ denotes all possible weighted-graph instances, as matrices, that may be formed over graphs with a vertex set, $A$, and edge weights taken from $\mathbb{R}'$.

Consider, for example, the graph in Figure \ref{fig:power-set-intuition-2-1} that has three edges with weights: 3.5, 9.1, and 6.9. This graph is shown in matrix form in Figure \ref{fig:power-set-intuition-2-2}. In Figure \ref{fig:power-set-intuition-2-3}, the function, $f : A \times A \mapsto \mathbb{R}'$, maps vertex pairs to edge weights for the graph. For example, we see that $(c, a)$ from set $A\times A$ maps to $6.9$ from set $\mathbb{R}'$. Likewise $(a, a)$ maps to $\bot$.

\begin{figure}[!t]
	\centering
	\subcaptionbox{\label{fig:power-set-intuition-2-1}A graph of three vertices, $a$, $b$, and $c$. Edge weights are selected from the extended real numbers.}
	[.45\textwidth]{
		\begin{tikzpicture}[]
			\begin{scope}[every node/.style={circle, fill, inner sep=0pt, minimum size=5pt, outer sep=4pt}]
				\node[label={south west:$a$}] (1) at ( 0.0, 0.0 ) {};
				\node[label={north:$b$}]      (2) at ( 1.5, 2.6 ) {};
				\node[label={south east:$c$}] (3) at ( 3.0, 0.0 ) {};
			\end{scope}
			\path [->] (1) edge[] node [left]  {$3.5$} (2);
			\path [->] (3) edge[] node [below] {$6.9$} (1);
			\path [->] (3) edge[] node [right] {$9.1$} (2);
		\end{tikzpicture}
	}
	\hfill%
	\subcaptionbox{\label{fig:power-set-intuition-2-2}A matrix view of the graph from Figure \ref{fig:power-set-intuition-2-1}.}
	[.45\textwidth]{
		\begin{tikzpicture}[]
			\matrix (M) [
				matrix of math nodes,
				left delimiter={(},
				right delimiter={)},
				ampersand replacement=\&,
				column sep=10pt,
				row sep=10pt
			] at ( 8.0, 1.3)
			{
				\bot \& 3.5  \& \bot \\
				\bot \& \bot \& \bot \\
				6.9  \& 9.1  \& \bot \\
			};
			\node[above=33pt of M-2-1, font=\scriptsize] (top-1)  {$a$};
			\node[above=33pt of M-2-2, font=\scriptsize] (top-2)  {$b$};
			\node[above=33pt of M-2-3, font=\scriptsize] (top-3)  {$c$};
			\node[left=75pt  of M-1-3, font=\scriptsize] (left-1) {$a$};
			\node[left=75pt  of M-2-3, font=\scriptsize] (left-2) {$b$};
			\node[left=75pt  of M-3-3, font=\scriptsize] (left-3) {$c$};
			\begin{pgfonlayer}{bg}
				\begin{scope}[every node/.style={circle, draw, highlightGray, fill, minimum size=20pt}]
					\node[] () at (M-1-2) {};
					\node[] () at (M-3-1) {};
					\node[] () at (M-3-2) {};
				\end{scope}
			\end{pgfonlayer}
		\end{tikzpicture}
	}
	\subcaptionbox{\label{fig:power-set-intuition-2-3}A particular function, $f$, from the set of functions $\mathbb{R}'^{A \times A}$ that denote possible matrices over index set $A$. Here, $f$ represents the weighted-graph instance from Figure \ref{fig:power-set-intuition-2-1}.}
	[1\textwidth]{
		\vspace{2em}%
		\begin{tikzpicture}[]
			\begin{scope}[every node/.style={inner sep=0pt, minimum size=5pt, outer sep=10pt}]
				\node[] (10) at ( 0.0,  2.0)  {$(a, a)$};
				\node[] (11) at ( 0.0,  3.0)  {$(a, c)$};
				\node[] (12) at ( 0.0,  4.0)  {$(b, a)$};
				\node[] (13) at ( 0.0,  5.0)  {$(b, b)$};
				\node[] (14) at ( 0.0,  6.0)  {$(b, c)$};
				\node[] (15) at ( 0.0,  7.0)  {$(c, c)$};
				\node[] (16) at ( 0.0,  8.0)  {$(a, b)$};
				\node[] (17) at ( 0.0,  9.0)  {$(c, a)$};
				\node[] (18) at ( 0.0,  10.0) {$(c, b)$};

				\node[] (20) at ( 5.5,  2.0)  {$\bot$};
				\node[] (21) at ( 5.5,  3.0)  {$\vdots$};
				\node[] (22) at ( 5.5,  4.0)  {$3.5$};
				\node[] (23) at ( 5.5,  5.0)  {$\vdots$};
				\node[] (24) at ( 5.5,  6.0)  {$6.9$};
				\node[] (25) at ( 5.5,  7.0)  {$\vdots$};
				\node[] (26) at ( 5.5,  8.0)  {$9.1$};
				\node[] (27) at ( 5.5,  9.0)  {$\vdots$};
				\node[] (28) at ( 5.5,  10.0) {$\top$};
			\end{scope}
			\node[] (l1) at ( 0.0,  0.0) {$A \times A$};
			\node[] (l2) at ( 5.5,  0.0) {$\mathbb{R}'$};
			\node[] (l3) at ($0.5*(l1) + 0.5*(l2) + (0, 12)$) {$f : A \times A \mapsto \mathbb{R}'$};

			\draw (14) ellipse (2.0 and 5.3);
			\draw (24) ellipse (2.0 and 5.3);

			\path [->] (10) edge node {} (20);
			\path [->] (11) edge node {} (20);
			\path [->] (12) edge node {} (20);
			\path [->] (13) edge node {} (20);
			\path [->] (14) edge node {} (20);
			\path [->] (15) edge node {} (20);
			\path [->] (16) edge node {} (22);
			\path [->] (17) edge node {} (24);
			\path [->] (18) edge node {} (26);
		\end{tikzpicture}
	}
	\caption[An intuition for how $\mathbb{R}'^{A \times A}$ denotes the set of weighted graphs.]{A depiction of how $\mathbb{R}'^{A \times A}$ denotes all possible weighted-graph instances, as matrices, that may be formed over graphs with a vertex set, $A$, and edge weights taken from $\mathbb{R}'$.}
	\label{fig:power-set-intuition-2}
\end{figure}