# Building the proof documentation

Ensure all theory files are in the `proofs/` directory. For example,
 - `Boruvka.thy`, 
 - `WCC.thy`
(Required files are listed in the `proofs/ROOT` file).

The main LaTeX template is `proofs/document/root.tex`

Create a PDF by executing the following command from the `proofs/` directory:

```sh
ISABELLE_DIR=/home/user/bin/Isabelle2020/bin
$ISABELLE_DIR/isabelle build -o browser_info -o document=pdf -o document_output=output -D .
```

The generated PDF will be `proofs/output/document.pdf` and its LaTeX sources will be in `proofs/output/document/`
