section \<open>Weakly connected components\<close>

text \<open>The results in this section are from \cite{guttmann2020verifying}.\<close>

theory WCC

imports Stone_Kleene_Relation_Algebras.Kleene_Relation_Algebras

begin

no_notation
  trancl ("(_\<^sup>+)" [1000] 999)

context stone_kleene_relation_algebra
begin

lemma reachable_without_loops:
  "x\<^sup>\<star> = (x \<sqinter> -1)\<^sup>\<star>"
proof (rule antisym)
  have "x * (x \<sqinter> -1)\<^sup>\<star> = (x \<sqinter> 1) * (x \<sqinter> -1)\<^sup>\<star> \<squnion> (x \<sqinter> -1) * (x \<sqinter> -1)\<^sup>\<star>"
    by (metis maddux_3_11_pp mult_right_dist_sup regular_one_closed)
  also have "... \<le> (x \<sqinter> -1)\<^sup>\<star>"
    by (metis inf.cobounded2 le_supI mult_left_isotone star.circ_circ_mult star.left_plus_below_circ star_involutive star_one)
  finally show "x\<^sup>\<star> \<le> (x \<sqinter> -1)\<^sup>\<star>"
    by (metis inf.cobounded2 maddux_3_11_pp regular_one_closed star.circ_circ_mult star.circ_sup_2 star_involutive star_sub_one)
next
  show "(x \<sqinter> -1)\<^sup>\<star> \<le> x\<^sup>\<star>"
    by (simp add: star_isotone)
qed

abbreviation "wcc x \<equiv> (x \<squnion> x\<^sup>T)\<^sup>\<star>"

lemma wcc_equivalence:
  "equivalence (wcc x)"
  apply (intro conjI)
  subgoal by (simp add: star.circ_reflexive)
  subgoal by (simp add: star.circ_transitive_equal)
  subgoal by (simp add: conv_dist_sup conv_star_commute sup_commute)
  done

lemma wcc_increasing:
  "x \<le> wcc x"
  by (simp add: star.circ_sub_dist_1)

lemma wcc_isotone:
  "x \<le> y \<Longrightarrow> wcc x \<le> wcc y"
  using conv_isotone star_isotone sup_mono by blast

lemma wcc_idempotent:
  "wcc (wcc x) = wcc x"
  using star_involutive wcc_equivalence by auto

lemma wcc_below_wcc:
  "x \<le> wcc y \<Longrightarrow> wcc x \<le> wcc y"
  using wcc_idempotent wcc_isotone by fastforce

lemma wcc_bot:
  "wcc bot = 1"
  by (simp add: star.circ_zero)

lemma wcc_one:
  "wcc 1 = 1"
  by (simp add: star_one)

lemma wcc_top:
  "wcc top = top"
  by (simp add: star.circ_top)

lemma wcc_with_loops:
  "wcc x = wcc (x \<squnion> 1)"
  using conv_dist_sup star_decompose_1 star_sup_one sup_commute symmetric_one_closed by presburger

lemma wcc_without_loops:
  "wcc x = wcc (x \<sqinter> -1)"
  by (metis conv_star_commute star_sum reachable_without_loops)

lemma forest_components_wcc:
  "injective x \<Longrightarrow> wcc x = forest_components x"
  by (simp add: cancel_separate_1)

abbreviation "fc x \<equiv> x\<^sup>\<star> * x\<^sup>T\<^sup>\<star>"

lemma fc_equivalence:
  "univalent x \<Longrightarrow> equivalence (fc x)"
  apply (intro conjI)
  subgoal by (simp add: reflexive_mult_closed star.circ_reflexive)
  subgoal by (metis cancel_separate_1 eq_iff star.circ_transitive_equal)
  subgoal by (simp add: conv_dist_comp conv_star_commute)
  done

lemma fc_increasing:
  "x \<le> fc x"
  by (metis le_supE mult_left_isotone star.circ_back_loop_fixpoint star.circ_increasing)

lemma fc_isotone:
  "x \<le> y \<Longrightarrow> fc x \<le> fc y"
  by (simp add: comp_isotone conv_isotone star_isotone)

lemma fc_idempotent:
  "univalent x \<Longrightarrow> fc (fc x) = fc x"
  by (metis fc_equivalence cancel_separate_1 star.circ_transitive_equal star_involutive)

lemma fc_star:
  "univalent x \<Longrightarrow> (fc x)\<^sup>\<star> = fc x"
  using fc_equivalence fc_idempotent star.circ_transitive_equal by simp

lemma fc_plus:
  "univalent x \<Longrightarrow> (fc x)\<^sup>+ = fc x"
  by (metis fc_star star.circ_decompose_9)

lemma fc_bot:
  "fc bot = 1"
  by (simp add: star.circ_zero)

lemma fc_one:
  "fc 1 = 1"
  by (simp add: star_one)

lemma fc_top:
  "fc top = top"
  by (simp add: star.circ_top)

lemma fc_wcc:
  "univalent x \<Longrightarrow> wcc x = fc x"
  by (simp add: fc_star star_decompose_1)

end

end

