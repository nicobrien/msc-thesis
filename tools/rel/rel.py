#!/bin/python3

def square(a):
    r = len(a)
    for i in range(r):
        if (len(a[i]) != r):
            return False
    return True

def empty(r, c):
    return [[0 for x in range(r)] for y in range(c)]

def bot(size):
    return [[0 for x in range(size)] for y in range(size)]

def top(size):
    return [[1 for x in range(size)] for y in range(size)]

def indentity(size):
    if size == 0:
        return []
    result = bot(size)
    for i in range(size):
        result[i][i] = 1
    return result

def copy(a):
    return [row[:] for row in a]

def tr(a):
    r = len(a)
    if r == 0:
        return []
    c = len(a[0])
    result = empty(r, c)
    for i in range(r):
        for j in range(c):
            result[i][j] = a[j][i]
    return result

def leq(a, b):
    la = len(a)
    lb = len(b)
    if la != lb:
        return False
    for i in range(la):
        if (len(a[i]) != len(b[i])):
            return False
        for j in range(len(a[i])):
            if not (a[i][j] <= b[i][j]):
                return False
    return True

def eq(a, b):
    la = len(a)
    lb = len(b)
    if la != lb:
        return False
    for i in range(la):
        if (len(a[i]) != len(b[i])):
            return False
        for j in range(len(a[i])):
            if (a[i][j] != b[i][j]):
                return False
    return True

def t_closure(a):
    r = len(a)
    if r == 0:
        return []
    if not square(a):
        return None
    previous = a
    an = compose(a, a)
    current = union(previous, an)
    while True:
        if eq(current, previous):
            break
        temp = copy(current)
        an = compose(an, a)
        current = union(current, an)
        previous = temp
    return current

def rt_closure(a):
    r = len(a)
    if r == 0:
        return []
    if not square(a):
        return None
    return union(t_closure(a), indentity(r))

def rts_closure(a):
    r = len(a)
    if r == 0:
        return []
    if not square(a):
        return None
    result = union(tr(a), a)
    return rt_closure(result)

def compose(a, b):
    la = len(a)
    lb = len(b)
    if la == 0 or lb == 0:
        return []
    if la != len(b[0]):
        return None
    result = [[0 for r in range(la)] for c in range(lb)]
    for i in range(la):
        for j in range(len(b[0])):
            for k in range(lb):
                result[i][j] = max(a[i][k] * b[k][j], result[i][j])
    return result

def union(a, b):
    la = len(a)
    lb = len(b)
    if not (square(a) and square(b)) or la != lb:
        return None
    result = bot(la)
    for i in range(la):
        for j in range(lb):
            result[i][j] = max(a[i][j], b[i][j])
    return result

def intersect(a, b):
    la = len(a)
    lb = len(b)
    if not (square(a) and square(b)) or la != lb:
        return None
    result = bot(la)
    for i in range(la):
        for j in range(lb):
            result[i][j] = min(a[i][j], b[i][j])
    return result

def complement(a):
    r = len(a)
    result = bot(r)
    for i in range(r):
        for j in range(len(a[0])):
            if a[i][j] == 0:
                result[i][j] = 1
            else:
                result[i][j] = 0
    return result


# for equivalence forest structures
def e_forest_children(x, d, c):
    return compose(compose(x, tr(d)), c)

def e_forest_descendants(x, d, c):
    return compose(t_closure(compose(x, tr(d))), c)

def e_forest_descendants_inclusive(x, d, c):
    return compose(rt_closure(compose(x, tr(d))), c)

def e_forest_ancestors(x, d, c):
    return compose(compose(x, t_closure(compose(d, x))), c)

def e_forest_ancestors_inclusive(x, d, c):
    return compose(compose(x, rt_closure(compose(d, x))), c)


def e_forest_roots(x, d):
    return complement(compose(t_closure(compose(x, tr(d))), top(len(x))))

def e_forest_root(x, d, c):
    return intersect(
        e_forest_roots(x, d),
        compose(rt_closure(compose(x, d)), c)
    )

def e_forest_siblings(x, d, c):
    return compose(compose(compose(compose(tr(compose(d, x)), x), d), x), c)
