#!/bin/python3

from pprint import pprint
from rel import *

c = [
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [1,1,1,1,1,1,1,1],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
]

d = [
    [0,0,1,0,0,0,0,0],
    [0,0,0,1,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,1,0,0],
    [0,0,0,0,0,0,1,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
]
d2 = [
    [0,0,0,0,0,0,0,0],
    [0,0,1,1,0,0,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,1,0,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,1,0],
    [0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0],
]

x = [
    [1,1,0,0,0,0,0,0],
    [1,1,0,0,0,0,0,0],
    [0,0,1,0,0,0,0,0],
    [0,0,0,1,1,0,0,0],
    [0,0,0,1,1,0,0,0],
    [0,0,0,0,0,1,0,0],
    [0,0,0,0,0,0,1,0],
    [0,0,0,0,0,0,0,1],
]


# siblings = e_forest_siblings(x, d, c)
# children = e_forest_children(x, d, c)
# descendants = e_forest_descendants(x, d, c)
# roots = e_forest_roots(x, d)
# root1 = e_forest_root(x, d2, c)
# root2 = e_forest_root(x, d2, children)
# ancestors = e_forest_ancestors(x, d, c)

# descendants = e_forest_descendants_inclusive(x, d2, c)
# r1 = e_forest_root(x, d2, c)
# r2 = e_forest_root(x, d2, descendants)

roots = e_forest_roots(x, d)
y = intersect(compose(rt_closure(compose(x, d)), c), roots)
z = intersect(compose(compose(rt_closure(compose(x, d)), rt_closure(compose(x, tr(d)))), c), roots)
pprint(y)
pprint(z)


