tex = '''
\\documentclass[10pt]{{article}}

\\usepackage[left=2cm, right=2cm, top=2cm, bottom=2cm, includefoot, footskip=30pt]{{geometry}}
\\usepackage{{tikz}}

\\begin{{document}}

\\title{{Generated Graph}}
\\author{{Nicolas Robinson-O'Brien}}
\\date{{}}
\\maketitle

\\section {{Graph}}

\\begin{{figure}}[h!]
    \\centering
    \\begin{{tikzpicture}}[auto,x=9pt,y=9pt]
{0}
    \\end{{tikzpicture}}
    \\caption{{Add caption here}}
    \\label{{fig:graph1}}
\\end{{figure}}

\\end{{document}}
'''

