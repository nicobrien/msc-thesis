import math
import random

class Point:
    
    def __init__(self, x, y):
        self.x = x
        self.y = y
        
        
    def on_segment(self, p, r):
        return self.x <= max(p.x, r.x) and self.x >= min(p.x, r.x) and self.y <= max(p.y, r.y) and self.y >= min(p.y, r.y)


    @staticmethod
    def orientation(p, q, r):
        ''' To find orientation of ordered triplet (p, q, r)
            The function returns following values 
             0  p, q and r are colinear
             1  Clockwise
            -1  Counterclockwise
        '''
        
        val = (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y); 
      
        if (val == 0):
            return 0     
        elif (val > 0):
            return 1
        else:
            return -1
        

class Vertex:
    
    def __init__(self, x, y, label = None):
        self.pos = Point(x, y)
        self.label = label
    
    
    def distance_to(self, vertex):
        return math.sqrt((self.pos.x - vertex.pos.x) ** 2 + (self.pos.y - vertex.pos.y) ** 2)
       
        
    def __str__(self):
        return "({}, {})".format(self.pos.x, self.pos.y)


class Edge:
    
    def __init__(self, start_vertex, end_vertex, label = None):
        self.start_vertex = start_vertex
        self.end_vertex = end_vertex
        self.label = None
        
        
    def length(self):
        return self.start_vertex.distance_to(self.end_vertex)
    
    
    def gradient(self):
        if (self.end_vertex.pos.x == self.start_vertex.pos.x):
            return 1
        else:
            return (self.end_vertex.pos.y - self.start_vertex.pos.y) / (self.end_vertex.pos.x - self.start_vertex.pos.x)
   
    
    def intersects_at_end(self, edge):
        p = self.start_vertex.pos
        q = self.end_vertex.pos
        r = edge.start_vertex.pos
        s = edge.end_vertex.pos

        intersect_at_end = (p.x == r.x and p.y == r.y) or (q.x == r.x and q.y == r.y) or (p.x == s.x and p.y == s.y) or (q.x == s.x and q.y == s.y)
        return intersect_at_end
    
    
    def intersects(self, edge):
        p = self.start_vertex.pos
        q = self.end_vertex.pos
        r = edge.start_vertex.pos
        s = edge.end_vertex.pos
        
        o1 = Point.orientation(p, q, r); 
        o2 = Point.orientation(p, q, s); 
        o3 = Point.orientation(r, s, p); 
        o4 = Point.orientation(r, s, q); 
  
        # general case
        if (o1 != o2 and o3 != o4):
            return True
  
        # p, q and r are collinear and r lies on segment pq
        if (o1 == 0 and r.on_segment(p, q)):
            return True
  
        # p, q and s are collinear and s lies on segment pq
        if (o2 == 0 and s.on_segment(p,q)):
            return True
  
        # r, s and p are collinear and p lies on segment rs 
        if (o3 == 0 and p.on_segment(r, s)):
            return True
          
        # r, s and q are collinear and q lies on segment rs 
        if (o4 == 0 and q.on_segment(r, s)):
            return True
  
        # doesn't fall into any of the above cases
        return False
    
    def __str__(self):
        return "{} - {}".format(self.start_vertex, self.end_vertex)


class Link:
    
    def __init__(self, c1, v1, c2, v2, label = None):
        self.c1 = c1
        self.c2 = c2
        self.v1 = v1
        self.v2 = v2
        self.label = label


class Component:

    def __init__(
        self,
        component_number,
        component_x_offset,
        component_y_offset,
        root_number,
        component_radius = 4.5,
        vertex_size = 5,
        vertex_ratio_from_centre = 0.713,
        number_of_vertices = 6,
        theta = 0):

        self.component_number = component_number
        self.component_x_offset = component_x_offset
        self.component_y_offset = component_y_offset
        self.component_radius = component_radius
        self.vertex_size = vertex_size
        
        self.vertex_ratio_from_centre = vertex_ratio_from_centre
        self.number_of_vertices = number_of_vertices
        self.theta = theta
        self.vertices = self.create_vertices()

        self.root = self.vertices[root_number]
        self.edges = self.create_path()


    def create_vertices(self):

        # center vertex
        vertices = []
        start = Vertex(self.component_x_offset, self.component_y_offset, str(self.component_number) + "0")
        vertices.append(start)

        # surrounding verticies
        for i in range(self.number_of_vertices):

            x = round(
                (math.cos(i * (2 * math.pi / self.number_of_vertices) + self.theta) * ((self.vertex_ratio_from_centre) * self.component_radius)) + self.component_x_offset,
                2
            )
            y = round(
                (math.sin(i * (2 * math.pi / self.number_of_vertices) + self.theta) * ((self.vertex_ratio_from_centre) * self.component_radius)) + self.component_y_offset,
                2
            )

            label = str(self.component_number) + str(i + 1)
            vertex = Vertex(x, y, label)
            vertices.append(vertex)
        return vertices


    def create_path(self):
        '''create a forest of edges'''

        edges = []
        considered = []
        remaining = self.vertices.copy()

        considered.append(self.root)
        remaining.remove(self.root)

        if self.number_of_vertices < 2:
            return edges

        # while nodes to consider
        while len(remaining) != 0:
            random_edge = self.get_non_crossing_random_edge(considered, remaining, edges)
            edges.append(random_edge)           
            considered.append(random_edge.end_vertex)
            remaining.remove(random_edge.end_vertex)
        return edges


    def get_non_crossing_random_edge(self, considered, remaining, edges):

        possible_start_vertices = considered.copy()
        done = False
        end_vertex = None
        while (not done and len(possible_start_vertices) != 0):
            start_vertex = random.choice(possible_start_vertices)
            edges_to_choose_from = [Edge(start_vertex, other_vertex ) for other_vertex in remaining]

            for possible in edges_to_choose_from.copy():
                for edge in edges:
                    if edge.intersects(possible) and not edge.intersects_at_end(possible):
                        edges_to_choose_from.remove(possible)
                        break

            if not len(edges_to_choose_from) == 0:
                end_vertex = random.choice(edges_to_choose_from).end_vertex
                done = True
            else:
                possible_start_vertices.remove(start_vertex)

        if end_vertex == None:
            return self.get_non_crossing_random_edge(considered, remaining, edges)

        return Edge(start_vertex, end_vertex)



