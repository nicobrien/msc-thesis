from component import Component, Link
from latex import Latex
import os
import math

filename = 'doc2'
out_directory_name = 'out'

try:
    os.makedirs(out_directory_name)
except FileExistsError:
    pass




def g1():
    c1 = Component(1, -7, 7, 5)
    c2 = Component(2, 10, 7, 2)
    c3 = Component(3, 24, 0, 0)
    c4 = Component(4, 10, -7, 2)
    c5 = Component(5, -7, -7, 3)

    components = [c1, c2, c3, c4, c5]
    links = [
        Link(c1, 2, c2, 7, "e"),
        Link(c1, 3, c2, 6, "q"),
        Link(c2, 3, c3, 7, "r"),
        Link(c3, 6, c4, 2, "s"),
        Link(c4, 6, c5, 3, "t"),
        Link(c5, 6, c1, 6, "u")
    ]

    latex = Latex(components, links, filename, out_directory_name)
    latex.write_and_open()


def g2():
    c1 = Component(1, 6.93, 3.99, 5)
    c2 = Component(2, 0, -8, 2)
    c3 = Component(3, -6.93, 3.99, 0)

    components = [c1, c2, c3]
    links = [
        Link(c1, 5, c2, 2, "e"),
        Link(c2, 7, c3, 4, "r"),
        Link(c3, 3, c1, 6, "s"),
    ]

    latex = Latex(components, links, filename, out_directory_name)
    latex.write_and_open()


def g3():
    c1 = Component(2, -7, 7, 5)
    c2 = Component(3, 7, 7, 2)
    c3 = Component(5, 7, -7, 2)
    c4 = Component(6, -7, -7, 3)

    components = [c1, c2, c3, c4]
    links = []

    latex = Latex(components, links, filename, out_directory_name)
    latex.write_and_open()

# g1()
# g2()
# g3()


def g4():

    c1 = Component(1, -6.00, 20.78, root_number=4)
    c2 = Component(2,  6.00, 20.78, root_number=2)
    c3 = Component(3,  0.00, 10.39, root_number=5)
    c4 = Component(4,  0.00, -1.61, root_number=0)
    c5 = Component(5, 10.39,  4.39, root_number=6)
    c6 = Component(6, 22.39,  4.39, root_number=6)

    components = [c1, c2, c3, c4, c5, c6]
    links = [
        Link(c1, 6, c3, 3, ""),
        Link(c2, 5, c3, 2, ""),
        Link(c3, 6, c5, 4, ""),
        Link(c4, 2, c5, 4, ""),
        Link(c5, 1, c6, 4, ""),
    ]

    latex = Latex(components, links, filename, out_directory_name)
    latex.write_and_open()


# g4()

def one():
    c0 = Component(0, 22.39,  4.39, root_number=6, component_radius=14)
    components = [c0]
    links = []
    latex = Latex(components, links, filename, out_directory_name)
    latex.write_and_open()

# one()

def hex():
    c0 = Component(0, 22.39,  4.39, root_number=6)
    c1 = Component(1, 35.22,  4.39, root_number=4)
    c2 = Component(2, 28.81, 15.50, root_number=2)
    c3 = Component(3, 15.97, 15.50, root_number=5)
    c4 = Component(4,  9.56,  4.39, root_number=0)
    c5 = Component(5, 15.97, -6.72, root_number=6)
    c6 = Component(6, 28.81, -6.72, root_number=6)

    components = [c0, c1, c2, c3, c4, c5, c6]
    links = [
        Link(c4, 1, c0, 5, ""),
        Link(c5, 2, c0, 5, ""),
        Link(c6, 3, c0, 5, ""),
        Link(c0, 2, c2, 5, ""),
        Link(c2, 6, c1, 3, ""),
        Link(c2, 4, c3, 1, ""),
    ]

    latex = Latex(components, links, filename, out_directory_name)
    latex.write_and_open()

# hex()


def h_forest():
    c1 = Component(1,  3,  0, root_number=4, theta=math.pi/2)
    c2 = Component(2,  3, 12, root_number=2, theta=math.pi/2)
    c3 = Component(3, 20, 12, root_number=5, theta=math.pi/2)
    c4 = Component(4, 20,  0, root_number=0, theta=math.pi/2)

    components = [c1, c2, c3, c4]
    links = [
        Link(c1, 1, c2, 4, ""),
        Link(c2, 1, c3, 1, ""),
        Link(c3, 4, c4, 1, ""),
    ]

    latex = Latex(components, links, filename, out_directory_name)
    latex.write_and_open()

# h_forest()

def t_forest():
    c1 = Component(1,  0,  0, root_number=4)
    c2 = Component(2,  0, 12, root_number=2)
    c3 = Component(3, 14,  0, root_number=0)
    c4 = Component(4, 14, 12, root_number=5)

    components = [c1, c2, c3, c4]
    links = [
        Link(c2, 1, c4, 4, "a"),
        Link(c3, 4, c1, 1, "b"),
        Link(c4, 1, c3, 1, ""),
    ]

    latex = Latex(components, links, filename, out_directory_name)
    latex.write_and_open()

#t_forest()



def t_forest_2():
    c1 = Component(1,  0,  0, root_number=4)
    c2 = Component(2,  0, 12, root_number=2)
    c3 = Component(3, 13,  0, root_number=0)
    c4 = Component(4, 13, 12, root_number=5)
    c5 = Component(5, 26,  0, root_number=3)
    c6 = Component(6, 26, 12, root_number=6)

    components = [c1, c2, c3, c4, c5, c6]
    links = [
        Link(c2, 1, c4, 4, "a"),
        Link(c3, 4, c1, 1, "b"),
        Link(c4, 6, c3, 2, ""),
        Link(c6, 4, c4, 1, ""),
    ]

    latex = Latex(components, links, filename, out_directory_name)
    latex.write_and_open()

#t_forest_2()

def t_forest_3():
    c1 = Component(1,    0,  0, root_number=4)
    c2 = Component(2,    0, 12, root_number=2)
    c3 = Component(3, 10.4,  6, root_number=4)
    c4 = Component(4, 20.8,  0, root_number=3)
    c5 = Component(5, 20.8, 12, root_number=0)

    components = [c1, c2, c3, c4, c5]
    links = [
        Link(c1, 2, c3, 4, "d_1"),
        Link(c2, 6, c3, 4, "d_2"),
        Link(c3, 1, c4, 3, ""),
    ]

    latex = Latex(components, links, filename, out_directory_name)
    latex.write_and_open()

t_forest_3()


