import os
import subprocess
from template import tex as template_tex

class Latex:
    
    def __init__(
        self,
        components,
        links,
        filename = 'doc',
        out_directory_name = 'out'):

        self.links = links
        self.components = components
        self.filename = filename
        self.out_directory_name = out_directory_name
        self.tex = None


    def generate_latex(self):
        content = "\n    \\begin{scope}[every node/.style={circle, fill, inner sep=0pt, minimum size=5pt, outer sep=4pt}]"
        for component in self.components:
            content += "\n\n        % Component {0} (start vertex {1})".format(component.component_number, component.root.label)
            content += self.generate_latex_vertices(component)
        content += "\n    \\end{scope}"
        for component in self.components:
            content += "\n"
            content += self.generate_latex_path(component)
        content += "\n"
        for component in self.components:
            content += self.generate_component_label(component)
        content += "\n"
        content += self.generate_links()
        self.tex = template_tex.format(content)


    def generate_links(self):
        content = ""
        for link in self.links:
            label = ""
            if link.label != "":
                label = "${0}$".format(link.label)
            content += "\n    \\path [->] ({0}{1}) edge [] node[] {{{2}}} ({3}{4});".format(
                link.c1.component_number,
                link.v1,
                label,
                link.c2.component_number,
                link.v2)
            
        return content

    def generate_component_label(self, component):
        content = "\n    \\node ({0}) at ({1}, {2}) {{$c_{3}$}};".format(
            "10" + str(component.component_number),
            component.component_x_offset,
            round(component.component_y_offset - component.component_radius - 1, 2),
            component.component_number
        )
        return content

    def generate_latex_vertices(self, component):
        content = ""
        fmt = "\n        \\node[] ({0}) at ({1:>6}, {2:>6}) {{}};"
        for vertex in component.vertices:
            content += fmt.format(vertex.label, vertex.pos.x, vertex.pos.y)
        content += "\n        \\draw ({0}, {1}) circle ({2});".format(
            component.component_x_offset, 
            component.component_y_offset, 
            component.component_radius
        )
        return content

    def generate_latex_path(self, component):
        content = ""
        for edge in component.edges:
            content += "\n    \\path [->] ({0}) edge node {{}} ({1});".format(
                edge.start_vertex.label,
                edge.end_vertex.label
            )
        return content

    def write_and_open(self):
        if self.tex == None:
            self.generate_latex()
        file_content = self.tex

        abs_out_directory = os.path.join(os.getcwd(), self.out_directory_name)
        tex_filename = self.filename + ".tex"   
        fh = os.path.join(abs_out_directory, tex_filename)
        print(fh)
        with open(fh, 'w') as f:
            f.write(file_content)

        proc = subprocess.Popen(['pdflatex', tex_filename], cwd=abs_out_directory)
        proc.communicate()

        abs_filepath = os.path.join(os.getcwd(), self.out_directory_name, self.filename)
        pdf_filename = abs_filepath + ".pdf"
        subprocess.Popen([pdf_filename], shell=True)
